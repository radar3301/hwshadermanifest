﻿using OpenTK.Graphics.OpenGL;
using System.Collections.Generic;
using System;

namespace HWShaderManifest
{
    public class Surface
    {
        internal bool ProtectBlendOps { get; set; }
        private int BlendSrcRgb, BlendDstRgb, BlendSrcAlpha, BlendDstAlpha;

        internal bool ProtectDepthWrite { get; set; }
        private bool DepthWritemask;

        internal bool ProtectDepthFunc { get; set; }
        private int DepthFunc;

        internal bool ProtectCull { get; set; }
        internal bool EnableCull;
        private int CullFaceMode;

        internal bool ProtectFill { get; set; }
        private int PolygonMode;

        internal bool ProtectClipPlane0 { get; set; }
        private bool ClipPlane0;

        internal bool ProtectColorWrite { get; set; }
        private bool[] ColorWritemask = new bool[4];

        internal bool ProtectOfsPolys { get; set; }
        private bool PolygonOffsetFill;

        internal bool ProtectOfsDepths { get; set; }
        private float PolygonOffsetFactor, PolygonOffsetUnits;

        internal bool ProtectPointSize { get; set; }
        private float PointSize;

        internal string Sort { get; set; } = "";
        internal bool NoTextures { get; set; } = false;
        internal bool DeathScissor { get; set; } = false;

        internal List<Layer> Layers = new List<Layer>();

        // uniform values defined in manifest
        internal Dictionary<string, dynamic> Locals = new Dictionary<string, dynamic>();

        // helper for on-the-fly changees
        internal Dictionary<string, dynamic> Uniforms = new Dictionary<string, dynamic>();
        internal Dictionary<string, int> Textures = new Dictionary<string, int>();

        public void LinkAttrib(string attrname, int buffer, int size, bool normalized)
        {
            foreach (Layer layer in Layers)
            {
                if (layer.Program == null) continue;
                int attr = layer.Program.GetAttribLocation(attrname);
                if (attr == -1) continue;
                GL.BindBuffer(BufferTarget.ArrayBuffer, buffer);
                GL.VertexAttribPointer(attr, size, VertexAttribPointerType.Float, normalized, 0, 0);
                GL.EnableVertexAttribArray(attr);
                Utils.GetGLError("Surface LinkAttrib");
            }
        }

        public void BindTexture(string name, int id)
        {
            Textures[name] = id;
        }

        internal void Protect(string str)
        {
            if (str == "blendOps") ProtectBlendOps = true;
            else if (str == "depthWrite") ProtectDepthWrite = true;
            else if (str == "depthFunc") ProtectDepthFunc = true;
            else if (str == "cull") ProtectCull = true;
            else if (str == "fill") ProtectFill = true;
            else if (str == "clipPlane0") ProtectClipPlane0 = true;
            else if (str == "colorWrite") ProtectColorWrite = true;
            else if (str == "ofsPolys") ProtectOfsPolys = true;
            else if (str == "ofsDepth") ProtectOfsDepths = true;
            else if (str == "pointSize") ProtectPointSize = true;
            else throw new ParseException("Unrecogized protection command `" + str + "`");
        }

        private void BeginProtect()
        {
            if (ProtectBlendOps)
            {
                BlendSrcRgb = GL.GetInteger(GetPName.BlendSrcRgb);
                BlendDstRgb = GL.GetInteger(GetPName.BlendDstRgb);
                BlendSrcAlpha = GL.GetInteger(GetPName.BlendSrcAlpha);
                BlendDstAlpha = GL.GetInteger(GetPName.BlendDstAlpha);
            }
            if (ProtectDepthWrite)
                DepthWritemask = GL.GetBoolean(GetPName.DepthWritemask);
            if (ProtectDepthFunc)
                DepthFunc = GL.GetInteger(GetPName.DepthFunc);
            if (ProtectCull)
            {
                EnableCull = GL.IsEnabled(EnableCap.CullFace);
                CullFaceMode = GL.GetInteger(GetPName.CullFaceMode);
            }
            if (ProtectFill)
                PolygonMode = GL.GetInteger(GetPName.PolygonMode);
            if (ProtectClipPlane0)
                ClipPlane0 = GL.IsEnabled(EnableCap.ClipPlane0);
            if (ProtectColorWrite)
                GL.GetBoolean(GetPName.ColorWritemask, ColorWritemask);
            if (ProtectOfsPolys)
                PolygonOffsetFill = GL.IsEnabled(EnableCap.PolygonOffsetFill);
            if (ProtectOfsDepths)
            {
                GL.GetFloat(GetPName.PolygonOffsetFactor, out PolygonOffsetFactor);
                GL.GetFloat(GetPName.PolygonOffsetUnits, out PolygonOffsetUnits);
            }
            if (ProtectPointSize)
                GL.GetFloat(GetPName.PointSize, out PointSize);
        }

        internal void AddLocal(List<string> args)
        {
            throw new NotImplementedException();
        }

        private void EndProtect()
        {
            if (ProtectBlendOps)
            {
                GL.BlendFuncSeparate(
                    (BlendingFactorSrc)BlendSrcRgb,
                    (BlendingFactorDest)BlendDstRgb,
                    (BlendingFactorSrc)BlendSrcAlpha,
                    (BlendingFactorDest)BlendDstAlpha);
            }
            if (ProtectDepthWrite)
                GL.DepthMask(DepthWritemask);
            if (ProtectDepthFunc)
                GL.DepthFunc((DepthFunction)DepthFunc);
            if (ProtectCull)
            {
                if (EnableCull)
                {
                    GL.Enable(EnableCap.CullFace);
                    GL.CullFace((CullFaceMode)CullFaceMode);
                }
                else
                    GL.Disable(EnableCap.CullFace);
            }
            if (ProtectFill)
                GL.PolygonMode(MaterialFace.FrontAndBack, (PolygonMode)PolygonMode);
            if (ProtectClipPlane0)
                if (ClipPlane0)
                    GL.Enable(EnableCap.ClipPlane0);
                else
                    GL.Disable(EnableCap.ClipPlane0);
            if (ProtectColorWrite)
                GL.ColorMask(ColorWritemask[0], ColorWritemask[1], ColorWritemask[2], ColorWritemask[3]);
            if (ProtectOfsPolys)
                if (PolygonOffsetFill)
                    GL.Enable(EnableCap.PolygonOffsetFill);
                else
                    GL.Disable(EnableCap.PolygonOffsetFill);
            if (ProtectOfsDepths)
                GL.PolygonOffset(PolygonOffsetFactor, PolygonOffsetUnits);
            if (ProtectPointSize)
                GL.PointSize(PointSize);
        }

        public void SetUniform(string name, dynamic value)
        {
            Uniforms[name] = value;
        }

        public void Draw(BeginMode mode, int count, DrawElementsType type, int indices)
        {
            BeginProtect();
            foreach (Layer layer in Layers)
            {
                if (layer.Program != null)
                {
                    layer.Apply();

                    foreach (KeyValuePair<string, dynamic> var in ShaderManifest.Globals)
                        layer[var.Key] = var.Value;
                    foreach (KeyValuePair<string, dynamic> var in Locals)
                        layer[var.Key] = var.Value;
                    foreach (ManifestUniform var in layer.Program.Locals.Values)
                        layer[var.Name] = var.Value;
                    foreach (KeyValuePair<string, dynamic> var in Uniforms)
                        layer[var.Key] = var.Value;
                    foreach (KeyValuePair<string, int> var in Textures)
                    {
                        string name = var.Key;
                        if (layer.Aliases.ContainsKey(name)) name = layer.Aliases[name];
                        if (layer.Program.Uniforms.ContainsKey(name) && layer.Program.Locals.ContainsKey(name))
                        {
                            Uniform uniform = layer.Program.Uniforms[name];
                            int unit = layer.Program.Locals[name].Value;
                            TextureUnit tunit = TextureUnit.Texture0 + unit;
                            GL.ActiveTexture(tunit);
                            if (uniform.type == ActiveUniformType.Sampler2D)
                                GL.BindTexture(TextureTarget.Texture2D, var.Value);
                            else if (uniform.type == ActiveUniformType.Sampler3D)
                                GL.BindTexture(TextureTarget.Texture3D, var.Value);
                            else if (uniform.type == ActiveUniformType.SamplerCube)
                                GL.BindTexture(TextureTarget.TextureCubeMap, var.Value);
                        }
                    }

                    GL.DrawElements(mode, count, type, indices);
                }
            }

            EndProtect();
        }

        internal class Layer
        {
            public bool BlendOps { get; internal set; }
            internal int blendSrc;
            internal int blendDest;

            public bool BlendOpsSeparate { get; internal set; }
            internal int blendSrcRGB;
            internal int blendDestRGB;
            internal int blendSrcAlpha;
            internal int blendDestAlpha;

            public bool DepthWrite { get; internal set; }
            internal bool depthWrite;

            public bool DepthFunc { get; internal set; }
            internal DepthFunction depthFunc;

            public bool Cull { get; internal set; }
            internal bool enableCull;
            internal CullFaceMode cull;

            public bool Fill { get; internal set; }
            internal int fill;

            public bool ClipPlane0 { get; internal set; }
            internal bool clipPlane0;

            public bool ColorWrite { get; internal set; }
            internal bool[] colorWrite = new bool[4];

            public bool OfsPolys { get; internal set; }
            internal bool ofsPolys;

            public bool OfsDepths { get; internal set; }
            internal float ofsFactor, ofsUnits;

            public bool PointSize { get; internal set; }
            internal float pointSize;

            private string programName;
            public string ProgramName
            {
                get { return programName; }
                internal set
                {
                    programName = value;
                    Program = ShaderManifest.GetProgram(programName);
                }
            }
            public ShaderProgram Program { get; set; }
            
            internal Dictionary<string, string> Aliases = new Dictionary<string, string>();
            internal Dictionary<string, dynamic> Locals = new Dictionary<string, dynamic>();

            internal void Apply()
            {
                //apply operations
                if (BlendOps)
                    GL.BlendFunc((BlendingFactor)blendSrc, (BlendingFactor)blendDest);
                if (BlendOpsSeparate)
                    GL.BlendFunc((BlendingFactor)blendSrc, (BlendingFactor)blendDest);
                if (DepthWrite)
                    GL.DepthMask(depthWrite);
                if (DepthFunc)
                    GL.DepthFunc(depthFunc);
                if (Cull)
                {
                    if (enableCull)
                    {
                        GL.Enable(EnableCap.CullFace);
                        GL.CullFace(cull);
                    }
                    else
                        GL.Disable(EnableCap.CullFace);
                }
                if (Fill)
                    GL.PolygonMode(MaterialFace.FrontAndBack, (PolygonMode)fill);
                if (ClipPlane0)
                    if (ClipPlane0)
                        GL.Enable(EnableCap.ClipPlane0);
                    else
                        GL.Disable(EnableCap.ClipPlane0);
                if (ColorWrite)
                    GL.ColorMask(colorWrite[0], colorWrite[1], colorWrite[2], colorWrite[3]);
                if (OfsPolys)
                    if (ofsPolys)
                        GL.Enable(EnableCap.PolygonOffsetFill);
                    else
                        GL.Disable(EnableCap.PolygonOffsetFill);
                if (OfsDepths)
                    GL.PolygonOffset(ofsFactor, ofsUnits);

                Program.Use();
            }

            internal dynamic this[string key]
            {
                set
                {
                    string dest = key;
                    if (Aliases.ContainsKey(key)) dest = Aliases[key];
                    Program.SetUniform(dest, value);
                }
            }
        }
    }
}