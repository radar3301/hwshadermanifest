﻿using System;

namespace HWShaderManifest
{
    [Serializable]
    internal class ManifestNotInitializedException : Exception
    {
        public ManifestNotInitializedException()
            : base("The shader manifest has not been initialized.")
        { }
    }

    [Serializable]
    internal class ParseException : Exception
    {
        public ParseException(string message) : base(message) { }
    }
}
