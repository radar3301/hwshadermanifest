﻿namespace HWShaderManifest
{
    internal class ConfigOption
    {
        private int _Value;
        public int DefaultValue { get; internal set; }
        public int MaxValue { get; internal set; } = int.MaxValue;
        public int Value
        {
            get
            {
                return _Value;
            }
            set
            {
                if (value < 0) value = 0;
                if (value > MaxValue) value = MaxValue;
                _Value = value;
            }
        }

        public bool CanUserModify { get; internal set; }
        public bool CanProgramModify { get; internal set; }

        internal ConfigOption(int value)
        {
            DefaultValue = _Value = value;
        }

        internal ConfigOption(int value, int defaultvalue, int maxvalue)
        {
            Value = value;
            DefaultValue = defaultvalue;
            MaxValue = value;
        }

        public void ResetToDefault()
        {
            _Value = DefaultValue;
        }
    }
}