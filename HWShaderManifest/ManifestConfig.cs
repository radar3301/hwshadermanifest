﻿using HWShaderManifest.ConfigParser;
using System;
using System.Collections.Generic;
using System.IO;

namespace HWShaderManifest
{
    public static class ManifestConfig
    {
        private static Dictionary<string, ConfigOption> options = new Dictionary<string, ConfigOption>();

        internal static void Load()
        {
            options.Clear();
            StreamReader stream = Utils.GetDataStream("shader config", @"shaders\dev_config.manifest");

            if (stream == null) return;

            // create the scanner to use
            Scanner scanner = new Scanner();

            // create the parser, and supply the scanner it should use
            Parser parser = new Parser(scanner);

            // define the input for the parser
            string input = stream.ReadToEnd();

            // parse the input. the result is a parse tree.
            ParseTree tree = parser.Parse(input);

            if (tree.Errors.Count == 0)
                tree.Execute();
        }

        internal static void Import(string importfile)
        {
            StreamReader stream = Utils.GetDataStream("imported config", @"shaders\" + importfile);

            if (stream == null) return;

            // create the scanner to use
            Scanner scanner = new Scanner();

            // create the parser, and supply the scanner it should use
            Parser parser = new Parser(scanner);

            // define the input for the parser
            string input = stream.ReadToEnd();

            // parse the input. the result is a parse tree.
            ParseTree tree = parser.Parse(input);

            if (tree.Errors.Count == 0)
                tree.Execute();
        }

        internal static void SetValueInternal(string name, int value)
        {
            if (options.ContainsKey(name))
                options[name].Value = value;
            else
                options.Add(name, new ConfigOption(value));
        }

        public static void SetValue(string name, int value)
        {
            if (options.ContainsKey(name))
                options[name].Value = value;
        }

        internal static void SetDefault(string name, int value)
        {
            if (options.ContainsKey(name))
                options[name].Value = value;
            else
            {
                ConfigOption option = new ConfigOption(value);
                option.DefaultValue = value;
                options.Add(name, option);
            }
        }

        internal static void SetMax(string name, int value)
        {
            if (options.ContainsKey(name))
                options[name].Value = value;
            else
            {
                ConfigOption option = new ConfigOption(value);
                option.MaxValue = value;
                options.Add(name, option);
            }
        }

        internal static void SetUserModify(string name, bool value)
        {
            if (options.ContainsKey(name))
                options[name].CanUserModify = value;
            else
                throw new InvalidOperationException();
        }

        internal static void SetProgramModify(string name, bool value)
        {
            if (options.ContainsKey(name))
                options[name].CanProgramModify = value;
            else
                throw new InvalidOperationException();
        }

        public static int GetValue(string name)
        {
            return options.ContainsKey(name) ? options[name].Value : 0;
        }

        public static int GetMaxValue(string name)
        {
            return options.ContainsKey(name) ? options[name].Value : 0;
        }

        public static int GetDefaultValue(string name)
        {
            return options.ContainsKey(name) ? options[name].Value : 0;
        }

        public static void ResetToDefaults()
        {
            foreach (ConfigOption option in options.Values)
                option.ResetToDefault();
        }

        public static List<string> GetOptions()
        {
            List<string> list = new List<string>();
            foreach (string name in options.Keys)
                list.Add(name);
            return list;
        }

        public static bool HasOption(string name)
        {
            return options.ContainsKey(name);
        }
    }
}