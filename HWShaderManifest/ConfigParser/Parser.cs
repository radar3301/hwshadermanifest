﻿using System;
using System.Collections.Generic;

namespace HWShaderManifest.ConfigParser
{
    #region Parser

    public partial class Parser
    {
        private Scanner scanner;
        private ParseTree tree;

        public Parser(Scanner scanner)
        {
            this.scanner = scanner;
        }

        public ParseTree Parse(string input)
        {
            tree = new ParseTree();
            return Parse(input, tree);
        }

        public ParseTree Parse(string input, ParseTree tree)
        {
            scanner.Init(input);

            this.tree = tree;
            ParseStart(tree);
            tree.Skipped = scanner.Skipped;

            return tree;
        }

        private void ParseStart(ParseNode parent)
        {
            Token tok;
            ParseNode n;
            ParseNode node = parent.CreateNode(scanner.GetToken(TokenType.Start), "Start");
            parent.Nodes.Add(node);



            ParseBlock(node);


            tok = scanner.Scan(TokenType.EOF);
            n = node.CreateNode(tok, tok.ToString());
            node.Token.UpdateRange(tok);
            node.Nodes.Add(n);
            if (tok.Type != TokenType.EOF)
            {
                tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.EOF.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                return;
            }

            parent.Token.UpdateRange(node.Token);
        }

        private void ParseBlock(ParseNode parent)
        {
            Token tok;
            ParseNode node = parent.CreateNode(scanner.GetToken(TokenType.Block), "Block");
            parent.Nodes.Add(node);

            tok = scanner.LookAhead(TokenType.SYM_IMPORT, TokenType.SYM_DEFINE, TokenType.SYM_DEFPROG, TokenType.SYM_DEFUSER, TokenType.SYM_IF);
            while (tok.Type == TokenType.SYM_IMPORT
                || tok.Type == TokenType.SYM_DEFINE
                || tok.Type == TokenType.SYM_DEFPROG
                || tok.Type == TokenType.SYM_DEFUSER
                || tok.Type == TokenType.SYM_IF)
            {
                ParseStatement(node);
                tok = scanner.LookAhead(TokenType.SYM_IMPORT, TokenType.SYM_DEFINE, TokenType.SYM_DEFPROG, TokenType.SYM_DEFUSER, TokenType.SYM_IF);
            }

            parent.Token.UpdateRange(node.Token);
        }

        private void ParseImportStat(ParseNode parent)
        {
            Token tok;
            ParseNode n;
            ParseNode node = parent.CreateNode(scanner.GetToken(TokenType.ImportStat), "ImportStat");
            parent.Nodes.Add(node);



            tok = scanner.Scan(TokenType.SYM_IMPORT);
            n = node.CreateNode(tok, tok.ToString());
            node.Token.UpdateRange(tok);
            node.Nodes.Add(n);
            if (tok.Type != TokenType.SYM_IMPORT)
            {
                tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.SYM_IMPORT.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                return;
            }


            tok = scanner.Scan(TokenType.STRING);
            n = node.CreateNode(tok, tok.ToString());
            node.Token.UpdateRange(tok);
            node.Nodes.Add(n);
            if (tok.Type != TokenType.STRING)
            {
                tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.STRING.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                return;
            }

            parent.Token.UpdateRange(node.Token);
        }

        private void ParseDefineStat(ParseNode parent)
        {
            Token tok;
            ParseNode n;
            ParseNode node = parent.CreateNode(scanner.GetToken(TokenType.DefineStat), "DefineStat");
            parent.Nodes.Add(node);



            tok = scanner.Scan(TokenType.SYM_DEFINE);
            n = node.CreateNode(tok, tok.ToString());
            node.Token.UpdateRange(tok);
            node.Nodes.Add(n);
            if (tok.Type != TokenType.SYM_DEFINE)
            {
                tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.SYM_DEFINE.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                return;
            }


            tok = scanner.Scan(TokenType.VAR);
            n = node.CreateNode(tok, tok.ToString());
            node.Token.UpdateRange(tok);
            node.Nodes.Add(n);
            if (tok.Type != TokenType.VAR)
            {
                tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.VAR.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                return;
            }


            tok = scanner.LookAhead(TokenType.VAR, TokenType.NUMBER);
            switch (tok.Type)
            {
                case TokenType.VAR:
                    tok = scanner.Scan(TokenType.VAR);
                    n = node.CreateNode(tok, tok.ToString());
                    node.Token.UpdateRange(tok);
                    node.Nodes.Add(n);
                    if (tok.Type != TokenType.VAR)
                    {
                        tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.VAR.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                        return;
                    }
                    break;
                case TokenType.NUMBER:
                    tok = scanner.Scan(TokenType.NUMBER);
                    n = node.CreateNode(tok, tok.ToString());
                    node.Token.UpdateRange(tok);
                    node.Nodes.Add(n);
                    if (tok.Type != TokenType.NUMBER)
                    {
                        tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.NUMBER.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                        return;
                    }
                    break;
                default:
                    tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found.", 0x0002, 0, tok.StartPos, tok.StartPos, tok.Length));
                    break;
            }

            parent.Token.UpdateRange(node.Token);
        }

        private void ParseDefineUserStat(ParseNode parent)
        {
            Token tok;
            ParseNode n;
            ParseNode node = parent.CreateNode(scanner.GetToken(TokenType.DefineUserStat), "DefineUserStat");
            parent.Nodes.Add(node);



            tok = scanner.Scan(TokenType.SYM_DEFUSER);
            n = node.CreateNode(tok, tok.ToString());
            node.Token.UpdateRange(tok);
            node.Nodes.Add(n);
            if (tok.Type != TokenType.SYM_DEFUSER)
            {
                tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.SYM_DEFUSER.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                return;
            }


            tok = scanner.Scan(TokenType.VAR);
            n = node.CreateNode(tok, tok.ToString());
            node.Token.UpdateRange(tok);
            node.Nodes.Add(n);
            if (tok.Type != TokenType.VAR)
            {
                tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.VAR.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                return;
            }

            parent.Token.UpdateRange(node.Token);
        }

        private void ParseDefineProgStat(ParseNode parent)
        {
            Token tok;
            ParseNode n;
            ParseNode node = parent.CreateNode(scanner.GetToken(TokenType.DefineProgStat), "DefineProgStat");
            parent.Nodes.Add(node);



            tok = scanner.Scan(TokenType.SYM_DEFPROG);
            n = node.CreateNode(tok, tok.ToString());
            node.Token.UpdateRange(tok);
            node.Nodes.Add(n);
            if (tok.Type != TokenType.SYM_DEFPROG)
            {
                tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.SYM_DEFPROG.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                return;
            }


            tok = scanner.Scan(TokenType.VAR);
            n = node.CreateNode(tok, tok.ToString());
            node.Token.UpdateRange(tok);
            node.Nodes.Add(n);
            if (tok.Type != TokenType.VAR)
            {
                tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.VAR.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                return;
            }

            parent.Token.UpdateRange(node.Token);
        }

        private void ParseStringCompare(ParseNode parent)
        {
            Token tok;
            ParseNode n;
            ParseNode node = parent.CreateNode(scanner.GetToken(TokenType.StringCompare), "StringCompare");
            parent.Nodes.Add(node);



            tok = scanner.Scan(TokenType.OP_STR);
            n = node.CreateNode(tok, tok.ToString());
            node.Token.UpdateRange(tok);
            node.Nodes.Add(n);
            if (tok.Type != TokenType.OP_STR)
            {
                tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.OP_STR.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                return;
            }


            tok = scanner.LookAhead(TokenType.OP_VENDOR, TokenType.OP_RENDERER);
            switch (tok.Type)
            {
                case TokenType.OP_VENDOR:
                    tok = scanner.Scan(TokenType.OP_VENDOR);
                    n = node.CreateNode(tok, tok.ToString());
                    node.Token.UpdateRange(tok);
                    node.Nodes.Add(n);
                    if (tok.Type != TokenType.OP_VENDOR)
                    {
                        tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.OP_VENDOR.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                        return;
                    }
                    break;
                case TokenType.OP_RENDERER:
                    tok = scanner.Scan(TokenType.OP_RENDERER);
                    n = node.CreateNode(tok, tok.ToString());
                    node.Token.UpdateRange(tok);
                    node.Nodes.Add(n);
                    if (tok.Type != TokenType.OP_RENDERER)
                    {
                        tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.OP_RENDERER.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                        return;
                    }
                    break;
                default:
                    tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found.", 0x0002, 0, tok.StartPos, tok.StartPos, tok.Length));
                    break;
            }


            tok = scanner.Scan(TokenType.STRING);
            n = node.CreateNode(tok, tok.ToString());
            node.Token.UpdateRange(tok);
            node.Nodes.Add(n);
            if (tok.Type != TokenType.STRING)
            {
                tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.STRING.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                return;
            }

            parent.Token.UpdateRange(node.Token);
        }

        private void ParseNumberCompare(ParseNode parent)
        {
            Token tok;
            ParseNode n;
            ParseNode node = parent.CreateNode(scanner.GetToken(TokenType.NumberCompare), "NumberCompare");
            parent.Nodes.Add(node);



            tok = scanner.LookAhead(TokenType.OP_GTE, TokenType.OP_LT);
            switch (tok.Type)
            {
                case TokenType.OP_GTE:
                    tok = scanner.Scan(TokenType.OP_GTE);
                    n = node.CreateNode(tok, tok.ToString());
                    node.Token.UpdateRange(tok);
                    node.Nodes.Add(n);
                    if (tok.Type != TokenType.OP_GTE)
                    {
                        tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.OP_GTE.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                        return;
                    }
                    break;
                case TokenType.OP_LT:
                    tok = scanner.Scan(TokenType.OP_LT);
                    n = node.CreateNode(tok, tok.ToString());
                    node.Token.UpdateRange(tok);
                    node.Nodes.Add(n);
                    if (tok.Type != TokenType.OP_LT)
                    {
                        tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.OP_LT.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                        return;
                    }
                    break;
                default:
                    tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found.", 0x0002, 0, tok.StartPos, tok.StartPos, tok.Length));
                    break;
            }


            tok = scanner.Scan(TokenType.OP_PART_ID);
            n = node.CreateNode(tok, tok.ToString());
            node.Token.UpdateRange(tok);
            node.Nodes.Add(n);
            if (tok.Type != TokenType.OP_PART_ID)
            {
                tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.OP_PART_ID.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                return;
            }


            tok = scanner.Scan(TokenType.NUMBER);
            n = node.CreateNode(tok, tok.ToString());
            node.Token.UpdateRange(tok);
            node.Nodes.Add(n);
            if (tok.Type != TokenType.NUMBER)
            {
                tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.NUMBER.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                return;
            }

            parent.Token.UpdateRange(node.Token);
        }

        private void ParseExpression(ParseNode parent)
        {
            Token tok;
            ParseNode node = parent.CreateNode(scanner.GetToken(TokenType.Expression), "Expression");
            parent.Nodes.Add(node);

            tok = scanner.LookAhead(TokenType.OP_STR, TokenType.OP_GTE, TokenType.OP_LT);
            switch (tok.Type)
            {
                case TokenType.OP_STR:
                    ParseStringCompare(node);
                    break;
                case TokenType.OP_GTE:
                case TokenType.OP_LT:
                    ParseNumberCompare(node);
                    break;
                default:
                    tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found.", 0x0002, 0, tok.StartPos, tok.StartPos, tok.Length));
                    break;
            }

            parent.Token.UpdateRange(node.Token);
        }

        private void ParseIfBlock(ParseNode parent)
        {
            Token tok;
            ParseNode n;
            ParseNode node = parent.CreateNode(scanner.GetToken(TokenType.IfBlock), "IfBlock");
            parent.Nodes.Add(node);



            tok = scanner.Scan(TokenType.SYM_IF);
            n = node.CreateNode(tok, tok.ToString());
            node.Token.UpdateRange(tok);
            node.Nodes.Add(n);
            if (tok.Type != TokenType.SYM_IF)
            {
                tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.SYM_IF.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                return;
            }


            ParseExpression(node);


            ParseBlock(node);


            tok = scanner.LookAhead(TokenType.SYM_ELSE);
            while (tok.Type == TokenType.SYM_ELSE)
            {


                tok = scanner.Scan(TokenType.SYM_ELSE);
                n = node.CreateNode(tok, tok.ToString());
                node.Token.UpdateRange(tok);
                node.Nodes.Add(n);
                if (tok.Type != TokenType.SYM_ELSE)
                {
                    tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.SYM_ELSE.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                    return;
                }


                tok = scanner.LookAhead(TokenType.OP_STR, TokenType.OP_GTE, TokenType.OP_LT);
                if (tok.Type == TokenType.OP_STR
                    || tok.Type == TokenType.OP_GTE
                    || tok.Type == TokenType.OP_LT)
                {
                    ParseExpression(node);
                }


                ParseBlock(node);
                tok = scanner.LookAhead(TokenType.SYM_ELSE);
            }


            tok = scanner.Scan(TokenType.SYM_ENDIF);
            n = node.CreateNode(tok, tok.ToString());
            node.Token.UpdateRange(tok);
            node.Nodes.Add(n);
            if (tok.Type != TokenType.SYM_ENDIF)
            {
                tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found. Expected " + TokenType.SYM_ENDIF.ToString(), 0x1001, 0, tok.StartPos, tok.StartPos, tok.Length));
                return;
            }

            parent.Token.UpdateRange(node.Token);
        }

        private void ParseStatement(ParseNode parent)
        {
            Token tok;
            ParseNode node = parent.CreateNode(scanner.GetToken(TokenType.Statement), "Statement");
            parent.Nodes.Add(node);

            tok = scanner.LookAhead(TokenType.SYM_IMPORT, TokenType.SYM_DEFINE, TokenType.SYM_DEFPROG, TokenType.SYM_DEFUSER, TokenType.SYM_IF);
            switch (tok.Type)
            {
                case TokenType.SYM_IMPORT:
                    ParseImportStat(node);
                    break;
                case TokenType.SYM_DEFINE:
                    ParseDefineStat(node);
                    break;
                case TokenType.SYM_DEFPROG:
                    ParseDefineProgStat(node);
                    break;
                case TokenType.SYM_DEFUSER:
                    ParseDefineUserStat(node);
                    break;
                case TokenType.SYM_IF:
                    ParseIfBlock(node);
                    break;
                default:
                    tree.Errors.Add(new ParseError("Unexpected token '" + tok.Text.Replace("\n", "") + "' found.", 0x0002, 0, tok.StartPos, tok.StartPos, tok.Length));
                    break;
            }

            parent.Token.UpdateRange(node.Token);
        }


    }

    #endregion Parser
}
