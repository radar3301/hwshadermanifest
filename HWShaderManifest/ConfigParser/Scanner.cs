﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace HWShaderManifest.ConfigParser
{
    #region Scanner

    public partial class Scanner
    {
        public string Input;
        public int StartPos = 0;
        public int EndPos = 0;
        public int CurrentLine;
        public int CurrentColumn;
        public int CurrentPosition;
        public List<Token> Skipped; // tokens that were skipped
        public Dictionary<TokenType, Regex> Patterns;

        private Token LookAheadToken;
        private List<TokenType> Tokens;
        private List<TokenType> SkipList; // tokens to be skipped

        public Scanner()
        {
            Regex regex;
            Patterns = new Dictionary<TokenType, Regex>();
            Tokens = new List<TokenType>();
            LookAheadToken = null;
            Skipped = new List<Token>();

            SkipList = new List<TokenType>();
            SkipList.Add(TokenType.COMMENT);
            SkipList.Add(TokenType.WHITESPACE);

            regex = new Regex(@"[0-9]+", RegexOptions.Compiled);
            Patterns.Add(TokenType.NUMBER, regex);
            Tokens.Add(TokenType.NUMBER);

            regex = new Regex(@"[A-Za-z_]+", RegexOptions.Compiled);
            Patterns.Add(TokenType.VAR, regex);
            Tokens.Add(TokenType.VAR);

            regex = new Regex(@"\S+", RegexOptions.Compiled);
            Patterns.Add(TokenType.STRING, regex);
            Tokens.Add(TokenType.STRING);

            regex = new Regex(@"#import", RegexOptions.Compiled);
            Patterns.Add(TokenType.SYM_IMPORT, regex);
            Tokens.Add(TokenType.SYM_IMPORT);

            regex = new Regex(@"#def", RegexOptions.Compiled);
            Patterns.Add(TokenType.SYM_DEFINE, regex);
            Tokens.Add(TokenType.SYM_DEFINE);

            regex = new Regex(@"#defprog", RegexOptions.Compiled);
            Patterns.Add(TokenType.SYM_DEFPROG, regex);
            Tokens.Add(TokenType.SYM_DEFPROG);

            regex = new Regex(@"#defuser", RegexOptions.Compiled);
            Patterns.Add(TokenType.SYM_DEFUSER, regex);
            Tokens.Add(TokenType.SYM_DEFUSER);

            regex = new Regex(@"#if", RegexOptions.Compiled);
            Patterns.Add(TokenType.SYM_IF, regex);
            Tokens.Add(TokenType.SYM_IF);

            regex = new Regex(@"#else", RegexOptions.Compiled);
            Patterns.Add(TokenType.SYM_ELSE, regex);
            Tokens.Add(TokenType.SYM_ELSE);

            regex = new Regex(@"#fi", RegexOptions.Compiled);
            Patterns.Add(TokenType.SYM_ENDIF, regex);
            Tokens.Add(TokenType.SYM_ENDIF);

            regex = new Regex(@"str", RegexOptions.Compiled);
            Patterns.Add(TokenType.OP_STR, regex);
            Tokens.Add(TokenType.OP_STR);

            regex = new Regex(@"gte", RegexOptions.Compiled);
            Patterns.Add(TokenType.OP_GTE, regex);
            Tokens.Add(TokenType.OP_GTE);

            regex = new Regex(@"lt", RegexOptions.Compiled);
            Patterns.Add(TokenType.OP_LT, regex);
            Tokens.Add(TokenType.OP_LT);

            regex = new Regex(@"vendor", RegexOptions.Compiled);
            Patterns.Add(TokenType.OP_VENDOR, regex);
            Tokens.Add(TokenType.OP_VENDOR);

            regex = new Regex(@"renderer", RegexOptions.Compiled);
            Patterns.Add(TokenType.OP_RENDERER, regex);
            Tokens.Add(TokenType.OP_RENDERER);

            regex = new Regex(@"part_id", RegexOptions.Compiled);
            Patterns.Add(TokenType.OP_PART_ID, regex);
            Tokens.Add(TokenType.OP_PART_ID);

            regex = new Regex(@"^$", RegexOptions.Compiled);
            Patterns.Add(TokenType.EOF, regex);
            Tokens.Add(TokenType.EOF);

            regex = new Regex(@"//[^\n]*\n?", RegexOptions.Compiled);
            Patterns.Add(TokenType.COMMENT, regex);
            Tokens.Add(TokenType.COMMENT);

            regex = new Regex(@"\s+", RegexOptions.Compiled);
            Patterns.Add(TokenType.WHITESPACE, regex);
            Tokens.Add(TokenType.WHITESPACE);


        }

        public void Init(string input)
        {
            this.Input = input;
            StartPos = 0;
            EndPos = 0;
            CurrentLine = 0;
            CurrentColumn = 0;
            CurrentPosition = 0;
            LookAheadToken = null;
        }

        public Token GetToken(TokenType type)
        {
            Token t = new Token(this.StartPos, this.EndPos);
            t.Type = type;
            return t;
        }

        /// <summary>
        /// executes a lookahead of the next token
        /// and will advance the scan on the input string
        /// </summary>
        /// <returns></returns>
        public Token Scan(params TokenType[] expectedtokens)
        {
            Token tok = LookAhead(expectedtokens); // temporarely retrieve the lookahead
            LookAheadToken = null; // reset lookahead token, so scanning will continue
            StartPos = tok.EndPos;
            EndPos = tok.EndPos; // set the tokenizer to the new scan position
            return tok;
        }

        /// <summary>
        /// returns token with longest best match
        /// </summary>
        /// <returns></returns>
        public Token LookAhead(params TokenType[] expectedtokens)
        {
            int i;
            int startpos = StartPos;
            Token tok = null;
            List<TokenType> scantokens;


            // this prevents double scanning and matching
            // increased performance
            if (LookAheadToken != null
                && LookAheadToken.Type != TokenType._UNDETERMINED_
                && LookAheadToken.Type != TokenType._NONE_) return LookAheadToken;

            // if no scantokens specified, then scan for all of them (= backward compatible)
            if (expectedtokens.Length == 0)
                scantokens = Tokens;
            else
            {
                scantokens = new List<TokenType>(expectedtokens);
                scantokens.AddRange(SkipList);
            }

            do
            {

                int len = -1;
                TokenType index = (TokenType)int.MaxValue;
                string input = Input.Substring(startpos);

                tok = new Token(startpos, EndPos);

                for (i = 0; i < scantokens.Count; i++)
                {
                    Regex r = Patterns[scantokens[i]];
                    Match m = r.Match(input);
                    if (m.Success && m.Index == 0 && ((m.Length > len) || (scantokens[i] < index && m.Length == len)))
                    {
                        len = m.Length;
                        index = scantokens[i];
                    }
                }

                if (index >= 0 && len >= 0)
                {
                    tok.EndPos = startpos + len;
                    tok.Text = Input.Substring(tok.StartPos, len);
                    tok.Type = index;
                }
                else if (tok.StartPos < tok.EndPos - 1)
                {
                    tok.Text = Input.Substring(tok.StartPos, 1);
                }

                if (SkipList.Contains(tok.Type))
                {
                    startpos = tok.EndPos;
                    Skipped.Add(tok);
                }
                else
                {
                    // only assign to non-skipped tokens
                    tok.Skipped = Skipped; // assign prior skips to this token
                    Skipped = new List<Token>(); //reset skips
                }
            }
            while (SkipList.Contains(tok.Type));

            LookAheadToken = tok;
            return tok;
        }
    }

    #endregion

    #region Token

    public enum TokenType
    {

        //Non terminal tokens:
        _NONE_ = 0,
        _UNDETERMINED_ = 1,

        //Non terminal tokens:
        Start = 2,
        Block = 3,
        ImportStat = 4,
        DefineStat = 5,
        DefineUserStat = 6,
        DefineProgStat = 7,
        StringCompare = 8,
        NumberCompare = 9,
        Expression = 10,
        IfBlock = 11,
        Statement = 12,

        //Terminal tokens:
        NUMBER = 13,
        VAR = 14,
        STRING = 15,
        SYM_IMPORT = 16,
        SYM_DEFINE = 17,
        SYM_DEFPROG = 18,
        SYM_DEFUSER = 19,
        SYM_IF = 20,
        SYM_ELSE = 21,
        SYM_ENDIF = 22,
        OP_STR = 23,
        OP_GTE = 24,
        OP_LT = 25,
        OP_VENDOR = 26,
        OP_RENDERER = 27,
        OP_PART_ID = 28,
        EOF = 29,
        COMMENT = 30,
        WHITESPACE = 31
    }

    public class Token
    {
        private int startpos;
        private int endpos;
        private string text;
        private object value;

        // contains all prior skipped symbols
        private List<Token> skipped;

        public int StartPos
        {
            get { return startpos; }
            set { startpos = value; }
        }

        public int Length
        {
            get { return endpos - startpos; }
        }

        public int EndPos
        {
            get { return endpos; }
            set { endpos = value; }
        }

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public List<Token> Skipped
        {
            get { return skipped; }
            set { skipped = value; }
        }
        public object Value
        {
            get { return value; }
            set { this.value = value; }
        }

        [XmlAttribute]
        public TokenType Type;

        public Token()
            : this(0, 0)
        {
        }

        public Token(int start, int end)
        {
            Type = TokenType._UNDETERMINED_;
            startpos = start;
            endpos = end;
            Text = ""; // must initialize with empty string, may cause null reference exceptions otherwise
            Value = null;
        }

        public void UpdateRange(Token token)
        {
            if (token.StartPos < startpos) startpos = token.StartPos;
            if (token.EndPos > endpos) endpos = token.EndPos;
        }

        public override string ToString()
        {
            if (Text != null)
                return Type.ToString() + " '" + Text + "'";
            else
                return Type.ToString();
        }
    }

    #endregion
}
