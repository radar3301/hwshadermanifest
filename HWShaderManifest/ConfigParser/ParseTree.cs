﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace HWShaderManifest.ConfigParser
{
    #region ParseTree
    [Serializable]
    public class ParseErrors : List<ParseError>
    {
    }

    [Serializable]
    public class ParseError
    {
        private string message;
        private int code;
        private int line;
        private int col;
        private int pos;
        private int length;

        public int Code { get { return code; } }
        public int Line { get { return line; } }
        public int Column { get { return col; } }
        public int Position { get { return pos; } }
        public int Length { get { return length; } }
        public string Message { get { return message; } }

        // just for the sake of serialization
        public ParseError()
        {
        }

        public ParseError(string message, int code, ParseNode node) : this(message, code, 0, node.Token.StartPos, node.Token.StartPos, node.Token.Length)
        {
        }

        public ParseError(string message, int code, int line, int col, int pos, int length)
        {
            this.message = message;
            this.code = code;
            this.line = line;
            this.col = col;
            this.pos = pos;
            this.length = length;
        }
    }

    // rootlevel of the node tree
    [Serializable]
    public partial class ParseTree : ParseNode
    {
        public ParseErrors Errors;

        public List<Token> Skipped;

        public ParseTree() : base(new Token(), "ParseTree")
        {
            Token.Type = TokenType.Start;
            Token.Text = "Root";
            Errors = new ParseErrors();
        }

        public string PrintTree()
        {
            StringBuilder sb = new StringBuilder();
            int indent = 0;
            PrintNode(sb, this, indent);
            return sb.ToString();
        }

        private void PrintNode(StringBuilder sb, ParseNode node, int indent)
        {

            string space = "".PadLeft(indent, ' ');

            sb.Append(space);
            sb.AppendLine(node.Text);

            foreach (ParseNode n in node.Nodes)
                PrintNode(sb, n, indent + 2);
        }

        /// <summary>
        /// Entry point for executing the parse tree.
        /// </summary>
        public new void Execute()
        {
            foreach (ParseNode node in nodes)
                node.Execute();
        }
    }

    [Serializable]
    [XmlInclude(typeof(ParseTree))]
    public partial class ParseNode
    {
        protected string text;
        protected List<ParseNode> nodes;

        public List<ParseNode> Nodes { get { return nodes; } }

        [XmlIgnore] // avoid circular references when serializing
        public ParseNode Parent;
        public Token Token; // the token/rule

        [XmlIgnore] // skip redundant text (is part of Token)
        public string Text
        { // text to display in parse tree 
            get { return text; }
            set { text = value; }
        }

        public virtual ParseNode CreateNode(Token token, string text)
        {
            ParseNode node = new ParseNode(token, text);
            node.Parent = this;
            return node;
        }

        protected ParseNode(Token token, string text)
        {
            this.Token = token;
            this.text = text;
            this.nodes = new List<ParseNode>();
        }

        /// <summary>
        /// this implements the evaluation functionality, cannot be used directly
        /// </summary>
        /// <param name="tree">the parsetree itself</param>
        /// <param name="paramlist">optional input parameters</param>
        /// <returns>a partial result of the evaluation</returns>
        internal void Execute()
        {
            switch (Token.Type)
            {
                case TokenType.Start:
                    ExecuteStart(); break;
                case TokenType.Block:
                    ExecuteBlock(); break;
                case TokenType.ImportStat:
                    ExecuteImportStat(); break;
                case TokenType.DefineStat:
                    ExecuteDefineStat(); break;
                case TokenType.DefineUserStat:
                    ExecuteDefineUserStat(); break;
                case TokenType.DefineProgStat:
                    ExecuteDefineProgStat(); break;
                case TokenType.StringCompare:
                    throw new InvalidOperationException();
                case TokenType.NumberCompare:
                    throw new InvalidOperationException();
                case TokenType.Expression:
                    throw new InvalidOperationException();
                case TokenType.IfBlock:
                    ExecuteIfBlock(); break;
                case TokenType.Statement:
                    ExecuteStatement(); break;
            }
        }

        private void ExecuteStart()
        {
            foreach (ParseNode childnode in nodes)
            {
                childnode.Execute();
            }
        }

        private void ExecuteBlock()
        {
            foreach (ParseNode childnode in nodes)
            {
                childnode.Execute();
            }
        }

        private void ExecuteImportStat()
        {
            string input = nodes[1].Token.Text;
            ManifestConfig.Import(input);
        }

        private void ExecuteDefineStat()
        {
            string name = nodes[1].Token.Text;
            int value;
            bool isnum = int.TryParse(nodes[2].Token.Text, out value);

            if (name.EndsWith("_MAX"))
            {
                name = name.Substring(0, name.IndexOf("_MAX"));
                ManifestConfig.SetMax(name, value);
            }
            else if (name.EndsWith("_DEF"))
            {
                name = name.Substring(0, name.IndexOf("_DEF"));
                ManifestConfig.SetDefault(name, isnum ? value : ManifestConfig.GetValue(name));
            }
            else
                ManifestConfig.SetValueInternal(name, value);
        }

        private void ExecuteDefineUserStat()
        {
            string name = nodes[1].Token.Text;
            ManifestConfig.SetUserModify(name, true);
        }

        private void ExecuteDefineProgStat()
        {
            string name = nodes[1].Token.Text;
            ManifestConfig.SetProgramModify(name, true);
        }

        private bool ExecuteStringCompare()
        {
            TokenType type = nodes[1].Token.Type;
            string str = nodes[2].Token.Text;
            if (type == TokenType.OP_VENDOR)
                return ShaderManifest.OpenGLVendor.Contains(str);
            else if (type == TokenType.OP_RENDERER)
                return ShaderManifest.OpenGLRenderer.Contains(str);
            else
                throw new InvalidOperationException();
        }

        private bool ExecuteNumberCompare()
        {
            TokenType op = nodes[0].Token.Type;
            TokenType left = nodes[1].Token.Type;
            string right = nodes[2].Token.Text;
            if (op == TokenType.OP_GTE)
            {
                if (left == TokenType.OP_PART_ID)
                    return ShaderManifest.OpenGLPartID >= int.Parse(right);
                else
                    throw new InvalidOperationException();
            }
            else if (op == TokenType.OP_LT)
            {
                if (left == TokenType.OP_PART_ID)
                    return ShaderManifest.OpenGLPartID < int.Parse(right);
                else
                    throw new InvalidOperationException();
            }
            else
                throw new InvalidOperationException();
        }

        private bool ExecuteExpression()
        {
            TokenType type = nodes[0].Token.Type;
            if (type == TokenType.StringCompare)
                return nodes[0].ExecuteStringCompare();
            else if (type == TokenType.NumberCompare)
                return nodes[0].ExecuteNumberCompare();
            else
                throw new InvalidOperationException();
        }

        private void ExecuteIfBlock()
        {
            if (nodes[1].ExecuteExpression())
            {
                nodes[2].Execute();
            }
            else
            {
                TokenType type;

                if (nodes[3].Token.Type == TokenType.SYM_ENDIF)
                    return;

                int i = 3;
                for (; i < nodes.Count; i += 3)
                {
                    type = nodes[i + 1].Token.Type;
                    if (type == TokenType.Expression)
                    {
                        if (nodes[i + 1].ExecuteExpression())
                        {
                            nodes[i + 2].Execute();
                            return;
                        }
                    }
                }

                type = nodes[i - 2].Token.Type;
                if (type == TokenType.Block)
                {
                    nodes[i - 2].Execute();
                }
                else
                    throw new InvalidOperationException();
            }
        }

        private void ExecuteStatement()
        {
            nodes[0].Execute();
        }
    }

    #endregion ParseTree
}
