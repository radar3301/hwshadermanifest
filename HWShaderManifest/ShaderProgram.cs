﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace HWShaderManifest
{
    internal class Attrib
    {
        public string name;
        public int address;
        public int size;
        public ActiveAttribType type;

        public Attrib(string name, int address, int size, ActiveAttribType type)
        {
            this.name = name;
            this.address = address;
            this.size = size;
            this.type = type;
        }
    }

    internal class Uniform
    {
        public string name;
        public int address;
        public int size;
        public ActiveUniformType type;

        public Uniform(string name, int address, int size, ActiveUniformType type)
        {
            this.name = name;
            this.address = address;
            this.size = size;
            this.type = type;
        }
    }

    internal class ShaderProgram
    {
        internal int ProgramID { get; private set; } = -1;
        internal int VShaderID { get; private set; } = -1;
        internal int FShaderID { get; private set; } = -1;

        internal string Filename { get; private set; }
        internal string FragShader = "";
        internal string VertShader = "";

        internal readonly Dictionary<string, Attrib> Attributes = new Dictionary<string, Attrib>();
        internal readonly Dictionary<string, Uniform> Uniforms = new Dictionary<string, Uniform>();
        internal readonly Dictionary<string, string> Globals = new Dictionary<string, string>();
        internal readonly Dictionary<string, ManifestUniform> Locals = new Dictionary<string, ManifestUniform>();

        internal ShaderProgram(string filename)
        {
            Filename = filename;
        }

        ~ShaderProgram()
        {
            //Delete();
        }

        internal int GetAttribLocation(string name)
        {
            return Attributes.ContainsKey(name) ? Attributes[name].address : -1;
        }

        public int GetUniformLocation(string name)
        {
            return Uniforms.ContainsKey(name) ? Uniforms[name].address : -1;
        }

        private bool CreateProgram(out int pID, out int vsID, out int fsID)
        {
            pID = 0;
            vsID = 0;
            fsID = 0;

            if (string.IsNullOrWhiteSpace(VertShader) || string.IsNullOrWhiteSpace(FragShader)) return false;

            Shader vShader = new Shader(ShaderType.VertexShader);
            Shader fShader = new Shader(ShaderType.FragmentShader);

            if (!vShader.Compile(VertShader)) return false;
            if (!fShader.Compile(FragShader)) return false;

            int programID = GL.CreateProgram();
            GL.AttachShader(programID, vShader.ID);
            GL.AttachShader(programID, fShader.ID);
            GL.LinkProgram(programID);

            int program_ok;
            GL.GetProgram(programID, GetProgramParameterName.LinkStatus, out program_ok);
            if (program_ok == 0)
            {
                ManifestLogger.WriteLine("Failed to link shaders to program:");
                ManifestLogger.WriteLine("--- DETAILS ---");
                ManifestLogger.WriteLine(GL.GetProgramInfoLog(programID));
                ManifestLogger.WriteLine("--- END ---");
                GL.DeleteProgram(programID);
                return false;
            }

            pID = programID;
            vsID = vShader.ID;
            fsID = fShader.ID;

            return true;
        }

        private void Delete()
        {
            if (ProgramID == -1) return;
            GL.DetachShader(ProgramID, VShaderID);
            GL.DetachShader(ProgramID, FShaderID);
            GL.DeleteProgram(ProgramID);
            GL.DeleteShader(VShaderID);
            GL.DeleteShader(FShaderID);
        }

        private void Link()
        {
            Attributes.Clear();
            Uniforms.Clear();

            int AttributeCount;
            int UniformCount;

            GL.GetProgram(ProgramID, GetProgramParameterName.ActiveAttributes, out AttributeCount);
            GL.GetProgram(ProgramID, GetProgramParameterName.ActiveUniforms, out UniformCount);

            for (int i = 0; i < AttributeCount; i++)
            {
                int length;
                int size;
                ActiveAttribType type;
                string name;

                GL.GetActiveAttrib(ProgramID, i, 256, out length, out size, out type, out name);

                Attrib attr = new Attrib(name, GL.GetAttribLocation(ProgramID, name), size, type);
                Attributes.Add(name, attr);
            }

            for (int i = 0; i < UniformCount; i++)
            {
                int length;
                int size;
                ActiveUniformType type;

                string name;

                GL.GetActiveUniform(ProgramID, i, 256, out length, out size, out type, out name);

                Uniform unif = new Uniform(name, GL.GetUniformLocation(ProgramID, name), size, type);
                Uniforms.Add(name, unif);
            }
        }

        public void Compile()
        {
            int pID, vsID, fsID;

            if (CreateProgram(out pID, out vsID, out fsID))
            {
                Delete();
                ProgramID = pID;
                VShaderID = vsID;
                FShaderID = fsID;
                Link();
            }
        }

        internal void Use()
        {
            if (ProgramID != 0)
                GL.UseProgram(ProgramID);
        }

        internal void SetUniform(string dest, dynamic var)
        {
            if (ProgramID == 0) return;

            if (Globals.ContainsKey(dest))
                dest = Globals[dest];
            if (!Uniforms.ContainsKey(dest))
            {
                if (Uniforms.ContainsKey(dest + "[0]"))
                    dest = dest + "[0]";
                else
                    return;
            }
            if (var is float[])
            {
                float[] arr = (float[])var;
                Uniform unif = Uniforms[dest];
                if (unif.type == ActiveUniformType.FloatVec2 && arr.Length == 2)
                    GL.Uniform2(Uniforms[dest].address, new Vector2(arr[0], arr[1]));
                else if (unif.type == ActiveUniformType.FloatVec3 && arr.Length == 3)
                    GL.Uniform3(Uniforms[dest].address, new Vector3(arr[0], arr[1], arr[2]));
                else if (unif.type == ActiveUniformType.FloatVec4 && arr.Length == 4)
                    GL.Uniform4(Uniforms[dest].address, new Vector4(arr[0], arr[1], arr[2], arr[3]));
                else if (unif.type == ActiveUniformType.FloatVec4 && (arr.Length % 4) == 0)
                {
                    GL.Uniform4(Uniforms[dest].address, arr.Length / 4, arr);
                }
                else
                    throw new Exception();
            }
            else if (var is int[])
            {
                int[] arr = (int[])var;
                Uniform unif = Uniforms[dest];
                if (unif.type == ActiveUniformType.IntVec2 && arr.Length == 2)
                    GL.Uniform2(Uniforms[dest].address, arr[0], arr[1]);
                else if (unif.type == ActiveUniformType.IntVec3 && arr.Length == 3)
                    GL.Uniform3(Uniforms[dest].address, new Vector3(arr[0], arr[1], arr[2]));
                else if (unif.type == ActiveUniformType.IntVec4 && arr.Length == 4)
                    GL.Uniform4(Uniforms[dest].address, new Vector4(arr[0], arr[1], arr[2], arr[3]));
                else
                    GL.Uniform1(Uniforms[dest].address, arr.Length, arr);
            }
            else if (var is float || var is int)
                GL.Uniform1(Uniforms[dest].address, var);
            else if (var is Vector2)
                GL.Uniform2(Uniforms[dest].address, var);
            else if (var is Vector3)
                GL.Uniform3(Uniforms[dest].address, var);
            else if (var is Vector4)
                GL.Uniform4(Uniforms[dest].address, var);
            else if (var is Matrix4)
            {
                Matrix4 mat = (Matrix4)var;
                GL.UniformMatrix4(Uniforms[dest].address, false, ref mat);
            }
            else if (var is Color4 || var is Color)
            {
                Color4 mat = (Color4)var;
                GL.Uniform4(Uniforms[dest].address, var);
            }
            else if (var is string)
            {
                // texture?
            }
            else
                throw new Exception();
            Utils.GetGLError("SetUniform");
        }

    }
}
