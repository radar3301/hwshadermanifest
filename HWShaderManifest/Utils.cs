﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.IO;

namespace HWShaderManifest
{
    internal static class Utils
    {
        public static T PopFirst<T>(this List<T> list)
        {
            T t = list[0];
            list.RemoveAt(0);
            return t;
        }

        public static T PopFirst<T>(ref T[] array)
        {
            T[] temp = array;
            Array.Resize(ref array, array.Length - 1);
            Array.ConstrainedCopy(temp, 1, array, 0, array.Length);
            T t = temp[0];
            return t;
        }

        public static string ReplaceAll(this string str, char oldChar, char newChar)
        {
            if (str.IndexOf(oldChar) == -1) return str;
            return ReplaceAll(str.Replace(oldChar, newChar), oldChar, newChar);
        }

        public static string ReplaceAll(this string str, string oldValue, string newValue)
        {
            if (str.IndexOf(oldValue) == -1) return str;
            return ReplaceAll(str.Replace(oldValue, newValue), oldValue, newValue);
        }

        /// <summary>
        /// Searches the list of defined datapaths for the specified file.
        /// </summary>
        /// <param name="datapath">The file to find.</param>
        /// <returns>The path of the specified file, if it exist; otherwise, returns an empty string.</returns>
        public static string ParseDataPath(string filename)
        {
            string filepath = "";
            foreach (string datapath in ShaderManifest.DataPaths)
            {
                string path = Path.Combine(datapath, filename);
                if (File.Exists(path))
                    filepath = path;
            }
            return filepath;
        }

        public static StreamReader GetDataStream(string type, string datapath)
        {
            StreamReader stream = null;
            string filepath = ParseDataPath(datapath);
            if (filepath == "")
                ManifestLogger.WriteLine(@"Unable to locate " + type + " file: " + datapath);
            else
                stream = new StreamReader(new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.Read));
            return stream;
        }

        public static bool GetLine(StreamReader sr, out string line)
        {
            line = "";
            try
            {
                line = sr.ReadLine();
                int comment = line.IndexOf("//");
                if (comment >= 0)
                    line = line.Substring(0, comment);
                line = line.Trim();
                line = line.ReplaceAll('\t', ' ');
                line = line.ReplaceAll("  ", " ");

                return true;
            }
            catch (Exception e)
            {
                if (sr.EndOfStream)
                    return false;
                ManifestLogger.WriteLine("Error reading file: " + e.Message);
                return false;
            }
        }

        public static void GetGLError(string type)
        {
            ErrorCode code = GL.GetError();
            if (code != ErrorCode.NoError)
                ManifestLogger.WriteLine(type + ": " + code);
        }
    }
}