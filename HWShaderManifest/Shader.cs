﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWShaderManifest
{
    internal class Shader
    {
        public ShaderType Type { get; private set; }

        public string Source { get; private set; } = "";
        public int ID { get; private set; } = 0;

        public Shader(ShaderType type)
        {
            Type = type;
        }

        public bool Compile(string source)
        {
            Source = source;
            ID = GL.CreateShader(Type);
            GL.ShaderSource(ID, source);
            GL.CompileShader(ID);
            
            int shader_ok;
            GL.GetShader(ID, ShaderParameter.CompileStatus, out shader_ok);

            if (shader_ok == 0)
            {
                ManifestLogger.WriteLine("Failed to compile " + Type.ToString() + " shader:");
                ManifestLogger.WriteLine("--- DETAILS ---");
                ManifestLogger.WriteLine(GL.GetShaderInfoLog(ID));
                ManifestLogger.WriteLine("--- END ---");
                GL.DeleteShader(ID);
                ID = 0;
                return false;
            }

            return true;
        }
    }
}
