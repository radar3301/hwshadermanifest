﻿namespace HWShaderManifest
{
    internal class ManifestUniform
    {
        public string Type { get; internal set; }
        public string Name { get; internal set; }
        public dynamic Value { get; set; }

        public ManifestUniform(string typename, string varname)
        {
            Type = typename;
            Name = varname;
        }
    }
}