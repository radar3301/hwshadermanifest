﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using HWShaderManifest;
using HWShaderManifest.Loaders.ProgramCompiler;

namespace HWShaderManifest.Loaders
{
    internal static class ProgramLoader
    {
        private static Interpreter interpreter;
        internal static ShaderProgram program;

        private static Dictionary<string, string> ProcessorDefines;

        public static ShaderProgram LoadProgram(string name)
        {
            string filename = @"shaders\gl_prog\" + name + ".prog";
            program = new ShaderProgram(filename);
            ReloadProgram(program);
            return program;
        }

        internal static void ReloadProgram(ShaderProgram prog)
        {
            ProcessorDefines = new Dictionary<string, string>();

            string filename = Utils.ParseDataPath(prog.Filename);
            Node code = Compiler.CompileSource(
                new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read));

            interpreter = new Interpreter(HandleIf);
            interpreter.RegisterFunction("debug", DebugReload);
            interpreter.RegisterFunction("add", AddTextScript);
            interpreter.RegisterFunction("compile", LoaderCompile);
            interpreter.RegisterFunction("local", AddLocal);
            interpreter.RegisterFunction("input", Input);
            interpreter.RegisterFunction("global", MapGlobal);
            interpreter.RegisterFunction("matrix", MapGlobal);
            interpreter.RegisterFunction("#import", Import);
            interpreter.RegisterFunction("#def", ProcessorDefine);
            interpreter.RegisterFunction("#undef", ProcessorUndefine);
            interpreter.RegisterFunction("debugReload", DebugReload);

            interpreter.Run(code);

        }

        private static int AddLocal(int nargs, Node args)
        {
            string typename = ((IdentNode)args[0]).identifier;
            string varname = ((IdentNode)args[1]).identifier;
            if (typename == "tex")
            {
                ManifestUniform local = new ManifestUniform(typename, varname);
                local.Value = int.Parse(((TextNode)args[2]).str);
                program.Locals[varname] = local;
                return 3;
            }
            else if (typename == "float")
            {
                ManifestUniform local = new ManifestUniform(typename, varname);
                local.Value = float.Parse(((TextNode)args[2]).str, CultureInfo.InvariantCulture);
                program.Locals[varname] = local;
                return 3;
            }
            else if (typename.StartsWith("float") || typename.StartsWith("vec"))
            {
                ManifestUniform local = new ManifestUniform(typename, varname);

                int length = int.Parse(typename.Substring(typename.StartsWith("float") ? 5 : 3));
                float[] array = new float[length];
                int i = 2;
                while (!(args[i] is EndOfLineNode))
                {
                    array[i - 2] = float.Parse(((TextNode)args[i]).str, CultureInfo.InvariantCulture);
                    i++;
                }
                local.Value = array;
                program.Locals[varname] = local;
                return i;
            }
            else
            {
                throw new Exception("Unrecognized type: " + typename);
            }
        }


        private static int ParseTextSource(Node args, out string source)
        {
            if (args[1] is FuncNode)
            {
                // handle special "#undef" case
                if (((FuncNode)args[1]).funcname == "#undef")
                {
                    source = "#undef " + ((TextNode)(((FuncNode)args[1]).arguments[0])).str;
                    return 2;
                }
                else
                {
                    throw new Exception();
                }
            }
            else
            {
                source = "";
                int i = 1;
                for (; !(args[i].Next is EndOfLineNode); i++)
                    source += ((TextNode)args[i]).str + " ";
                source += ((TextNode)args[i]).str;
                return i + 1;
            }
        }

        private static int AddTextScript(int nargs, Node args)
        {
            string dest = ((IdentNode)args[0]).identifier;

            string source;
            int ret = ParseTextSource(args, out source);

            if (dest == "vert_text")
            {
                program.VertShader += source + "\r\n";
                return ret;
            }
            else if (dest == "geo_text")
            {
                //program.GeomShader += source + "\r\n";
                return ret;
            }
            else if (dest == "frag_text" || dest == "frag_text_")
            {
                program.FragShader += source + "\r\n";
                return ret;
            }
            else if (dest == "vert_script")
            {
                string scriptfile = Utils.ParseDataPath(@"shaders\gl_prog\" + source);
                if (scriptfile != null)
                {
                    StreamReader sr = new StreamReader(
                        new FileStream(scriptfile, FileMode.Open, FileAccess.Read, FileShare.Read));
                    program.VertShader += sr.ReadToEnd();
                    sr.Close();
                }
                return 2;
            }
            else if (dest == "geom_script")
            {
                string scriptfile = Utils.ParseDataPath(@"shaders\gl_prog\" + source);
                if (scriptfile != null)
                {
                    StreamReader sr = new StreamReader(
                        new FileStream(scriptfile, FileMode.Open, FileAccess.Read, FileShare.Read));
                    //program.GeomShader += sr.ReadToEnd();
                    sr.Close();
                }
                return 2;
            }
            else if (dest == "frag_script")
            {
                string scriptfile = Utils.ParseDataPath(@"shaders\gl_prog\" + source);
                if (scriptfile != null)
                {
                    StreamReader sr = new StreamReader(
                        new FileStream(scriptfile, FileMode.Open, FileAccess.Read, FileShare.Read));
                    program.FragShader += sr.ReadToEnd();
                    sr.Close();
                }
                return 2;
            }
            else
                throw new Exception("Illegal destination.");

        }

        private static int LoaderCompile(int nargs, Node args)
        {
            program.Compile();
            return 0;
        }

        private static int Input(int nargs, Node args)
        {
            int i = 0;
            for (Node n = args; !(n is EndOfLineNode); n = n.Next, i++)
            {
                // Inputs.Add();
            }
            return i;
        }

        private static int MapGlobal(int nargs, Node args)
        {
            string globalsrc = ((IdentNode)args[0]).identifier;
            string uniformdest = ((IdentNode)args[1]).identifier;
            program.Globals[globalsrc] = uniformdest;
            return 2;
        }

        private static int Import(int nargs, Node args)
        {
            if (nargs != 1) throw new Exception("`#import' expected 1 argument, found " + nargs);
            string impname = ((TextNode)args[0]).str;

            string path = Utils.ParseDataPath(@"shaders\gl_prog\" + impname);
            if (path == null) return 0; // throw new Exception("Unable to locate file: shaders\\dev_config.manifest");
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            Node code = Compiler.CompileSource(fs);
            interpreter.Run(code);

            return 0;
        }

        private static int ProcessorDefine(int nargs, Node args)
        {
            string arg0 = ((TextNode)args[0]).str;
            string arg1 = nargs > 1 ? ((TextNode)args[1]).str : "";
            ProcessorDefines[arg0] = arg1;
            return 0;
        }

        private static int ProcessorUndefine(int nargs, Node args)
        {
            string arg0 = ((TextNode)args[0]).str;
            ProcessorDefines.Remove(arg0);
            return 0;
        }

        private static int DebugReload(int nargs, Node args)
        {
            //ignore
            return 1;
        }

        private static bool HandleIf(BinOpr op, string left, string right)
        {
            switch (op)
            {
                case BinOpr.OP_NE:
                    break;
                case BinOpr.OP_EQ:
                    break;
                case BinOpr.OP_LT:
                    break;
                case BinOpr.OP_LE:
                    break;
                case BinOpr.OP_GT:
                    break;
                case BinOpr.OP_GE:
                    int num = int.Parse(right);
                    return ManifestConfig.GetValue(left) >= num;
                case BinOpr.OP_STR:
                    break;
                case BinOpr.OP_DEF:
                    return ManifestConfig.HasOption(left) || ProcessorDefines.ContainsKey(left);
            }
            throw new Exception();
        }
    }
}