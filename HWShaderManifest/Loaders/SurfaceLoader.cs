﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HWShaderManifest.Utils;

namespace HWShaderManifest.Loaders
{
    internal static class SurfaceLoader
    {
        private enum Keyword
        {
            Protect,
            Local,
            Layer,
            Sort,
            NoTextures,
            DeathScissor,
            NonKeyword
        }

        private static Surface surface;

        internal static Surface LoadSurface(string name)
        {
            string path = ParseDataPath(@"shaders\gl_surf\" + name + ".surf");
            if (string.IsNullOrEmpty(path))
            {
                ManifestLogger.WriteLine(@"Unable to locate surface: shaders\gl_surf\" + name + ".surf");
                return null;
            }
            else
            {
                try
                {
                    surface = new Surface();
                    using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
                    using (StreamReader sr = new StreamReader(fs))
                        Parse(new StreamReader(new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read)));
                    return surface;
                }
                catch (ParseException ex)
                {
                    ManifestLogger.WriteLine(ex.Message);
                    return null;
                }
            }
        }

        private static Keyword IsKeyword(string str)
        {
            if (str == "protect") return Keyword.Protect;
            if (str == "local") return Keyword.Local;
            if (str == "layer") return Keyword.Layer;
            if (str == "sort") return Keyword.Sort;
            if (str == "notextures") return Keyword.NoTextures;
            if (str == "deathscissor") return Keyword.DeathScissor;
            return Keyword.NonKeyword;
        }

        private static Surface.Layer CurrentLayer; // helper for parser

        private static void Parse(StreamReader sr)
        {
            Keyword mode = Keyword.NonKeyword;
            string line;
            while (!sr.EndOfStream && GetLine(sr, out line))
            {
                if (string.IsNullOrWhiteSpace(line)) continue;
                List<string> args = new List<string>(line.Split(new char[] { ' ' }));
                string str = args[0];
                Keyword word = IsKeyword(str);
                if (word != Keyword.NonKeyword) args.PopFirst();
                switch (word)
                {
                    case Keyword.Protect:
                        mode = Keyword.Protect;
                        break;
                    case Keyword.Local:
                        string loc_name;
                        string loc_type;
                        dynamic loc_val;
                        ParseLocal(args, out loc_name, out loc_type, out loc_val);
                        //ManifestUniform local = new ManifestUniform(loc_type, loc_name);
                        if (mode != Keyword.Layer)
                            surface.Locals[loc_name] = loc_val;
                        else
                            CurrentLayer.Locals[loc_name] = loc_val;
                        break;
                    case Keyword.Layer:
                        mode = Keyword.Layer;
                        CurrentLayer = new Surface.Layer();
                        surface.Layers.Add(CurrentLayer);
                        break;
                    case Keyword.Sort:
                        surface.Sort = args[0];
                        break;
                    case Keyword.NoTextures:
                        surface.NoTextures = true;
                        break;
                    case Keyword.DeathScissor:
                        surface.DeathScissor = true;
                        break;
                    case Keyword.NonKeyword:
                        if (mode == Keyword.Protect) surface.Protect(str);
                        if (mode == Keyword.Layer) Layer(args);
                        break;
                }
            }
            sr.Close();
            sr.Dispose();
        }

        private static void ParseLocal(List<string> args, out string loc_name, out string typename, out dynamic loc_val)
        {
            if (args.Count < 3) throw new ArgumentException();
            typename = args.PopFirst();
            loc_name = args.PopFirst();

            if (typename == "tex")
            {
                loc_val = args[0];
            }
            else if (typename == "float")
            {
                loc_val = float.Parse(args[0], CultureInfo.InvariantCulture);
            }
            else if (typename.StartsWith("float"))
            {
                int length = int.Parse(typename.Substring(5));
                float[] array = new float[length];
                for (int i = 0; i < args.Count; i++)
                    array[i] = float.Parse(args[i], CultureInfo.InvariantCulture);
                loc_val = array;
            }
            else if (typename.StartsWith("vec"))
            {
                int length = int.Parse(typename.Substring(3));
                float[] array = new float[length];
                for (int i = 0; i < args.Count; i++)
                    array[i] = float.Parse(args[i], CultureInfo.InvariantCulture);
                if (length == 2) loc_val = new Vector2(array[0], array[1]);
                else if (length == 3) loc_val = new Vector3(array[0], array[1], array[2]);
                else if (length == 4) loc_val = new Vector4(array[0], array[1], array[2], array[3]);
                else throw new ParseException("Invalid number of parameters for " + typename);
            }
            else
            {
                throw new ParseException("Unrecognized type: " + typename);
            }
        }

        private static int getBlendingFactor(string str)
        {
            if (str == "zero") return 0;
            if (str == "one") return 1;
            if (str == "srcColor") return 768;
            if (str == "invSrcColor" || str == "srcInvColor") return 769;
            if (str == "srcAlpha") return 770;
            if (str == "srcInvAlpha" || str == "invSrcAlpha") return 771;
            if (str == "dstAlpha") return 772;
            if (str == "dstInvAlpha" || str == "invDstAlpha") return 773;
            if (str == "dstColor") return 774;
            if (str == "invDstColor" || str == "dstInvColor") return 775;
            if (str == "alphaSat" || str == "satAlpha") return 776;
            if (str == "constColor") return 32769;
            if (str == "invConstColor" || str == "constInvColor" || str == "invColor") return 32770;
            if (str == "constAlpha") return 32771;
            if (str == "invConstAlpha" || str == "constInvAlpha" || str == "invAlpha") return 32772;

            throw new ParseException("Unrecognized BlendingFactor: " + str);
        }

        private static EnableCap getEnableCap(string str)
        {
            if (str == "DepthTest") return EnableCap.DepthTest;
            throw new ParseException("Unrecognized EnableCap: " + str);
        }

        private static DepthFunction getDepthFunc(string str)
        {
            if (str == "always") return DepthFunction.Always;
            if (str == "less") return DepthFunction.Less;
            if (str == "lequal") return DepthFunction.Lequal;
            if (str == "gequal") return DepthFunction.Gequal;
            throw new ParseException("Unrecognized DepthFunction: " + str);
        }

        private static void Layer(List<string> args)
        {
            string str = args.PopFirst();
            if (str == "program")
            {
                CurrentLayer.ProgramName = args.PopFirst();
                CurrentLayer.Program = ShaderManifest.GetProgram(CurrentLayer.ProgramName);
            }
            else if (str == "blendOps")
            {
                if (args.Count == 2)
                {
                    CurrentLayer.BlendOps = true;
                    CurrentLayer.blendSrc = getBlendingFactor(args[0]);
                    CurrentLayer.blendDest = getBlendingFactor(args[1]);
                }
                else if (args.Count == 4)
                {
                    CurrentLayer.BlendOpsSeparate = true;
                    CurrentLayer.blendSrcRGB = getBlendingFactor(args[0]);
                    CurrentLayer.blendDestRGB = getBlendingFactor(args[1]);
                    CurrentLayer.blendSrcAlpha = getBlendingFactor(args[2]);
                    CurrentLayer.blendDestAlpha = getBlendingFactor(args[3]);
                }
            }
            else if (str == "depthWrite")
            {
                CurrentLayer.DepthWrite = true;
                string val = args[0];
                if (val == "0" || val == "false")
                    CurrentLayer.depthWrite = false;
                else if (val == "1" || val == "true")
                    CurrentLayer.depthWrite = true;
                else
                    throw new ParseException("Unrecognized value (" + args[0] + ") for " + str);
            }
            else if (str == "depthFunc")
            {
                CurrentLayer.DepthFunc = true;
                CurrentLayer.depthFunc = getDepthFunc(args[0]);
            }
            else if (str == "cull")
            {
                CurrentLayer.Cull = true;
                if (args[0] == "none")
                    CurrentLayer.enableCull = false;
                else if (args[0] == "back")
                {
                    CurrentLayer.enableCull = true;
                    CurrentLayer.cull = CullFaceMode.Back;
                }
                else if (args[0] == "front")
                {
                    CurrentLayer.enableCull = true;
                    CurrentLayer.cull = CullFaceMode.Front;
                }
                else
                    throw new ParseException("Unrecognized value (" + args[0] + ") for " + str);
            }
            else if (str == "fill")
            {
                CurrentLayer.Fill = true;
                CurrentLayer.fill = (int)PolygonMode.Fill;
            }
            else if (str == "clipPlane0")
            {
                // unknown what effect this has
                CurrentLayer.ClipPlane0 = true;
                if (args[0] == "1")
                    CurrentLayer.clipPlane0 = true;
                else if (args[0] == "0")
                    CurrentLayer.clipPlane0 = false;
                else
                    throw new ParseException("Unrecognized value (" + args[0] + ") for " + str);
            }
            else if (str == "colorWrite")
            {
                CurrentLayer.ColorWrite = true;
                CurrentLayer.colorWrite[0] = args[0] == "1";
                CurrentLayer.colorWrite[1] = args[1] == "1";
                CurrentLayer.colorWrite[2] = args[2] == "1";
                CurrentLayer.colorWrite[3] = args[3] == "1";
            }
            else if (str == "ofsPolys")
            {
                CurrentLayer.OfsPolys = true;
                string val = args[0];
                if (val == "0")
                    CurrentLayer.ofsPolys = false;
                else if (val == "1")
                    CurrentLayer.ofsPolys = true;
                else
                    throw new ParseException("Unrecognized value (" + args[0] + ") for " + str);
            }
            else if (str == "ofsDepths")
            {
                CurrentLayer.OfsDepths = true;
                float factor = 0f;
                float units = 0f;
                if (args.Count >= 1) factor = float.Parse(args[0], CultureInfo.InvariantCulture);
                if (args.Count >= 2) units = float.Parse(args[1], CultureInfo.InvariantCulture);
                CurrentLayer.ofsFactor = factor;
                CurrentLayer.ofsUnits = units;
            }
            else if (str == "pointSize")
            {
                CurrentLayer.PointSize = true;
                float size = 0.001f;
                if (args.Count >= 1) size = float.Parse(args[0], CultureInfo.InvariantCulture);
                CurrentLayer.pointSize = size;
            }
            else if (str == "alias")
            {
                CurrentLayer.Aliases[args[0]] = args[1];
            }
            else
            {
                throw new ParseException("Unrecognized layer operation: " + str);
            }
        }
    }
}