﻿using System.IO;
using System.Reflection;

namespace HWShaderManifest
{
    internal static class ManifestLogger
    {
        /// <summary>
        /// Location of the log file.
        /// </summary>
        public static readonly string LogPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "manifest.log");
        private static StreamWriter stream;

        public static void Init()
        {
            if (stream != null)
                Close();
            stream = new StreamWriter(new FileStream(LogPath, FileMode.Create, FileAccess.Write, FileShare.Read));
        }

        private static void Reopen()
        {
            stream = new StreamWriter(new FileStream(LogPath, FileMode.Append, FileAccess.Write, FileShare.Read));
        }

        public static void WriteLine()
        {
            if (stream == null) Reopen();
            stream.WriteLine();
        }

        public static void WriteLine(string value)
        {
            if (stream == null) Reopen();
            stream.WriteLine(value);
        }

        public static void Close()
        {
            if (stream != null)
            {
                stream.Flush();
                stream.Close();
                stream = null;
            }
        }
    }
}