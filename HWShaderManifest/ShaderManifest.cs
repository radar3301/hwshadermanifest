﻿using HWShaderManifest.Loaders;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace HWShaderManifest
{
    public static class ShaderManifest
    {
        private static Regex IntegerRegex = new Regex(@"\d+");
        private static bool IsInitialized;

        /// <summary>
        /// List of paths to search for files under.
        /// Paths with higher indices have a higher priority than those with lower indices.
        /// </summary>
        public static List<string> DataPaths { get; } = new List<string>();

        /// <summary>
        /// List of default texture IDs.
        /// </summary>
        public static Dictionary<string, int> DefaultTextures { get; } = new Dictionary<string, int>();

        /// <summary>
        /// Gets the version or release number of the OpenGL implementation.
        /// </summary>
        public static string OpenGLVersion { get; private set; }
        /// <summary>
        /// Gets the company responsible for the OpenGL implementation.
        /// </summary>
        public static string OpenGLVendor { get; private set; }
        /// <summary>
        /// Gets the name of the renderer.
        /// This name is typically specific to a particular configuration of a hardware platform.
        /// </summary>
        public static string OpenGLRenderer { get; private set; }
        /// <summary>
        /// Gets the part ID of the renderer.
        /// </summary>
        public static int OpenGLPartID { get; private set; }
        
        internal static Dictionary<string, dynamic> Globals = new Dictionary<string, dynamic>();
        private static Dictionary<string, string> HODAliases = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

        private static Dictionary<string, Surface> Surfaces = new Dictionary<string, Surface>(StringComparer.InvariantCultureIgnoreCase);
        private static Dictionary<string, ShaderProgram> Programs = new Dictionary<string, ShaderProgram>();

        public static void Init()
        {
            if (IsInitialized) return;
            IsInitialized = true;
            
            ManifestLogger.Init();
            ManifestLogger.WriteLine("GL Info: " + (OpenGLVersion = GL.GetString(StringName.Version)));
            ManifestLogger.WriteLine("Vendor: " + (OpenGLVendor = GL.GetString(StringName.Vendor)));
            ManifestLogger.WriteLine("Renderer: " + (OpenGLRenderer = GL.GetString(StringName.Renderer)));
            ManifestLogger.WriteLine("PartID: " + (OpenGLPartID = int.Parse(IntegerRegex.Match(OpenGLRenderer).Value)));

            Reload();
        }

        public static void Reload()
        {
            if (!IsInitialized) Init();

            //Globals.Clear();
            Surfaces.Clear();

            ManifestConfig.Load();

            StreamReader stream = Utils.GetDataStream("manifest", @"shaders\master.manifest");
            if (stream != null)
                Load(stream);

            ManifestLogger.Close();
        }

        private static void Load(StreamReader stream)
        {
            while (!stream.EndOfStream)
            {
                string line;
                if (!Utils.GetLine(stream, out line))
                {
                    ManifestLogger.WriteLine("End of stream reached while parsing file.");
                    return;
                }
                if (string.IsNullOrWhiteSpace(line)) continue;

                if (line.StartsWith("#import")) Import(line.Substring(8));
                else if (line.StartsWith("global")) AddGlobal(line.Substring(7));
                else if (line.StartsWith("load program")) { }
                else if (line.StartsWith("load surface")) { }
                else if (line.StartsWith("hod_alias")) AddHODAlias(line.Substring(9));
                else ManifestLogger.WriteLine("Unrecognized manifest command: " + line);
            }
            stream.Close();
        }

        private static void Import(string file)
        {
            StreamReader stream = Utils.GetDataStream("imported manifest", Path.Combine("shaders", file));
            if (stream != null)
                Load(stream);
        }

        private static void AddGlobal(string input)
        {
            string[] args = input.Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string typename = Utils.PopFirst(ref args);
            string varname = Utils.PopFirst(ref args);
            int i = 0;
            if (typename.StartsWith("float"))
            {
                int length = int.Parse(typename.Substring(5));
                float[] array = new float[length];
                while (args.Length > 0)
                    array[i++] = float.Parse(Utils.PopFirst(ref args), CultureInfo.InvariantCulture);
                //Globals[varname] = array;
            }
            else if (typename.StartsWith("int"))
            {
                int length = int.Parse(typename.Substring(3));
                int[] array = new int[length];
                while (args.Length > 0)
                    array[i++] = int.Parse(Utils.PopFirst(ref args), CultureInfo.InvariantCulture);
                //Globals[varname] = array;
            }
            else
                ManifestLogger.WriteLine("Unrecognized type: " + typename);
        }

        private static void AddHODAlias(string input)
        {
            string[] args = input.Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string surf = args[0];
            string shad = args[1];
            string pref = args[2];

            HODAliases[shad] = surf;
        }

        /// <summary>
        /// Gets the named surface, loading it if necessary.
        /// </summary>
        public static Surface GetSurface(string name)
        {
            if (!IsInitialized) throw new ManifestNotInitializedException();
            if (HODAliases.ContainsKey(name)) name = HODAliases[name];
            if (Surfaces.ContainsKey(name)) return Surfaces[name];
            
            return Surfaces[name] = SurfaceLoader.LoadSurface(name);
        }

        /// <summary>
        /// Gets the named program, loading it if necessary.
        /// </summary>
        internal static ShaderProgram GetProgram(string name)
        {
            if (!IsInitialized) throw new ManifestNotInitializedException();
            if (Programs.ContainsKey(name)) return Programs[name];
            
            return Programs[name] = ProgramLoader.LoadProgram(name);
        }

        public static string[] GetHODAliases()
        {
            string[] array = new string[HODAliases.Keys.Count];
            HODAliases.Keys.CopyTo(array, 0);
            return array;
        }

        public static bool ContainsHODAlias(string alias)
        {
            return HODAliases.ContainsKey(alias);
        }

        public static void SetGlobalUniform(string name, dynamic value)
        {
            Globals[name] = value;
        }
    }
}
